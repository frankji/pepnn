# PepNN

PepNN is a deep learning model for the identification of peptide binding sites given an input protein and a peptide sequence that makes use of a novel attention-based module. There are two variants of the model: PepNN-Struct, which takes as input a protein structure, and PepNN-Seq, which takes as input a protein sequence. The model can also make peptide-agnostic predictions, allowing for the identification of novel peptide binding modules.

## Installation

Its recommended that you install `pepnn` in a clean python virtual environment.

1. Install git-lfs `conda install -c conda-forge git-lfs && git lfs install` 
2. Clone the repository `git clone https://gitlab.com/oabdin/pepnn.git`
3. Install requirements `pip install -r requirements.txt`
4. Install package `pip install .`

## Peptide binding site prediction using PepNN

### PepNN-Struct

To run the test for PepNN-Struct, move to the example file directory (`cd example`) and run the following command

`python ../pepnn_struct/scripts/predict_binding_site.py -prot 4oih.pdb -pep 4oih_peptide.fasta -c A`

The script takes as input the location of a PDB file with a protein structure (`-prot`), an (optional) fasta file with a peptide sequence (`-pep`) and the the chain id of the protein in the PDB file (`-c`). An output folder will be generated with a csv containing the binding probabilities at each residue, a txt file with the overall score for the domain and a PDB file with binding probabilities in the B-factor column.

### PepNN-Seq

To run the test for PepNN-Struct, move to the example file directory (`cd example`) and run the following command

`python ../pepnn_struct/scripts/predict_binding_site.py -prot 4oih_A.fasta -pep 4oih_peptide.fasta `

The script takes as input the location of a fasta file with a protein sequence (`-prot`), an (optional) fasta file with a peptide sequence (`-pep`). An output folder will be generated with a csv containing the binding probabilities at each residue and a txt file with the overall score for the domain.

## Reference

Osama Abdin, Han Wen, Philip M Kim. _PepNN: a deep attention model for the identification of peptide binding sites_. https://doi.org/10.1101/2021.01.10.426132 
